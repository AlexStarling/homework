function typeChecker (elem) {
    if (typeof elem === 'object') {
        return extractFromObject(elem);
    } else {
        return `<li>${elem}</li>`;
    }
}

function extractFromObject (obj) {
    let listElements = "";

    for (let key in obj) {
        listElements += typeChecker(obj[key]);
    }

    let list = `<ul>${listElements}</ul>`;

    return list;
}

function createList (arr) {
    const listElements = arr.map(item => {
        return typeChecker(item);
    }).join("");

    let list = `<ul>${listElements}</ul>`;

    return list;
}

document.body.innerHTML = createList(
    ['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv', ['1', '2', '3', 'sea', 'user', 23, {"1": "Borispol",
        "41": "Irpin"}]]);

// timer

let counter = 10;
const timer = document.createElement("P");
timer.style.fontSize = '40px';
timer.textContent = `:${counter}`;


document.body.prepend(timer);

function finalCountdown () {
    counter--;
    timer.textContent = `:${counter}`;

    if (counter < 10) {
        timer.textContent = `:0${counter}`;
    }
}

setInterval(finalCountdown, 1000);


function eraseHTML () {
    document.write("");
}

setTimeout(eraseHTML, 1000 * 10);